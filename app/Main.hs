module Main (main) where

type Ands = [Ors]
type Ors = [(Int,Bool)]

main :: IO ()
main = do
        file <- readFile "uf20-01.sat"
        let as = (fmap . fmap) (\(i,b) -> (i - 1,b)) $ parse file
        let combinations = allCombinations $ maxInt as + 1
        let results = zip combinations $ fmap (evaluate . insert as) combinations
        let solutions = filter (\(_,b) -> b == True) results
        putStrLn $ "Done! Solutions found: " ++ show (length solutions)
        putStrLn $ unlines $ fmap (show . \(as,a) -> (fmap display as, display a)) $ solutions

allCombinations :: Int -> [[Bool]]
allCombinations 1 = [[False],[True]]
allCombinations n = fmap ((:) False) next ++ fmap ((:) True) next
    where next = allCombinations $ n - 1

parse :: String -> Ands
parse = (fmap . fmap) (\s -> if head s == '!' then (read $ tail s,True) else (read s,False)) . fmap words . lines

maxInt :: Ands -> Int
maxInt = maximum . fmap maximum . (fmap . fmap) fst

insert :: Ands -> [Bool] -> [[Bool]]
insert as bs = (fmap . fmap) (\(i,b) -> (if b then not else id) $ bs !! i) as

evaluate :: [[Bool]] -> Bool
evaluate a = foldr (&&) True $ fmap (foldr (||) False) a

display :: Bool -> Int
display False = 0
display True = 1
